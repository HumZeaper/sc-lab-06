package No02;

public abstract class Cars {
	private String name;
	public Cars(String name){
		this.name = name;
	}

	public abstract String engine();
	
	public String toString(){
		return this.name;
	}
}
